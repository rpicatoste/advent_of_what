import numpy as np

op= {1: lambda x,y: x+y,
     2: lambda x,y: x*y,}

start_data = np.genfromtxt('day_2_input.csv', delimiter=',', dtype=int)
expected_output = 19690720

for ii in range(100):
    for jj in range(100):
        my_data = start_data.copy()
        my_data[1], my_data[2] = ii, jj

        idx = 0
        while my_data[idx] != 99:
            my_data[my_data[idx+3]] = op[my_data[idx]](my_data[my_data[idx+1]], my_data[my_data[idx+2]])
            idx += 4

        if my_data[0] == expected_output:
            print(f'Solution: {ii}, {jj} -> {100*ii + jj}')
            break