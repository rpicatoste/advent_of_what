import pandas as pd
input_data = pd.read_csv('day_1_input.csv', header=None)
output = (input_data/3).astype(int) - 2
print(output.sum()[0])

to_add = output.copy()
while to_add.sum()[0] > 0:
    to_add = ((to_add/3).astype(int) - 2).clip(lower=0)
    output += to_add

print(output.sum()[0])
